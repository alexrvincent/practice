function myFunc(...args) {
	console.log(this);
    console.log(args);
}

let myObj = {
    key: 'value',
    itsFunction: () => {
        let that = this;
        console.log(that);
    }
}
 
 myFunc.call(myObj, 1,2,3);
 myFunc.apply(myObj, [1,2,3]);

 let myBoundedFunc = myFunc.bind(myObj);
 myBoundedFunc(1,2,3);

 myObj.itsFunction();
