var theThree= 3;

var addThreeTo = function(number) {

    var theThree = 10;

    return function() {
        console.log(number + theThree);
    }
}

var myEquation = addThreeTo(1);
var myEquation2 = addThreeTo(2);

myEquation();
myEquation2();


// Note the inner 'theThree' is the one used instead of the outer. This is because the closure
// of the inner function finds the variable it looks for from inward out and uses it first.