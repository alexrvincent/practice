/* Create a function batches that returns the maximum number of whole batches that can be cooked from a recipe. 


It accepts two objects as arguments: the first object is the recipe
for the food, while the second object is the available ingredients.
Each ingredient's value is number representing how many units there are.

`batches(recipe, available)`

ex: 

// 0 batches can be made
batches(
  { milk: 100, butter: 50, flour: 5 },
  { milk: 132, butter: 48, flour: 51 }
)

// 1 batch can be made
batches(
  { milk: 100, butter: 50, cheese: 10 },
  { milk: 198, butter: 52, cheese: 10 }
)

*/

const batches = (recipe, available=null) => {

    let possibleBatches = 0;

    if (available === null) return 0;

    for (let ingredient in recipe) {

        if(!available[ingredient]) return 0;

        else if(available[ingredient] / recipe[ingredient] < 1) return 0;

        else {
            if (possibleBatches === 0) possibleBatches = Math.floor(available[ingredient] / recipe[ingredient]);
            else {
                possibleBatches = Math.min(possibleBatches, Math.floor(available[ingredient] / recipe[ingredient]))
            }
        }

    }

    return possibleBatches;
}

// Zero
console.log(`${batches(
    { milk: 100, butter: 50, flour: 5 },
    { milk: 132, butter: 48, flour: 51 }
)} batches can be made`);

// Zero
console.log(`${batches(
    { milk: 100, flour: 4, sugar: 10, butter: 5 },
    { milk: 1288, flour: 9, sugar: 95 }
)} batches can be made`);

// One
console.log(`${batches(
    { milk: 100, butter: 50, cheese: 10 },
    { milk: 198, butter: 52, cheese: 10 }
)} batches can be made`);

// Two
console.log(`${batches(
    { milk: 2, sugar: 40, butter: 20 },
    { milk: 5, sugar: 120, butter: 500 }
)} batches can be made`);

// 1) Use Object.keys to get an array of the recipe keys
// 2) Run a map function on that array that compares the values at the keys for both objects and places
// their division differences in an array
// 3) Use the spread operator to split that array into a sequence of values
// 4) Pass that sequence of values to the Math.min operator
// 5) Optionally round down the value to the nearest whole value.

const batches2 = (recipe, available) =>
  Math.floor(
    Math.min(...Object.keys(recipe).map(k => available[k] / recipe[k] || 0))
  )