let x = {
    a: 1,
    b: 2
}

const convertObjectToArray = (obj) => {
    let arr = [];
    for (let item in obj) arr.push(obj[item]);
    return arr;
}

const convertObjectToArray2 = (obj) => {
    return Object.values(obj)
}

console.log(convertObjectToArray(x));
console.log(convertObjectToArray2(x));