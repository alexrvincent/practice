// I want to run [1,2].print() and have it print 1,2 
// How would I do this?

// Solution 1
Array.prototype.print = function() { 

    let result = '';

    for(let i = 0; i < this.length; ++i) {
        (i === this.length - 1) ? result += this[i] : result += this[i] + ","
    }

    console.log(result);

};


// Solution 2
Array.prototype.print = function() { 
    console.log(String(this));
};

[1, 2].print();
