function example() {
    console.log(this);
}

const bind = (fn, context) => {
    return (...args) => fn.apply(context, args)
}
  
const boundExample = bind(example, { a: true });

boundExample.call({ b: true }) // logs { a: true }
