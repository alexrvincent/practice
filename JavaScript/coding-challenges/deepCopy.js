let obj1 = {
    notNested: 'a',
    nested: {
        a: 'b',
        nested: {
            nested: {
                nested: {
                    nested: {
                        nested: {
                            nested: {
                                superNested: "I am super nested!"
                            },
                        },
                    },
                },
            },
            
        },
    },
    aFunction: function() {
        console.log(this.nested.a);
    },
    aFunction2: function() {
        console.log(this.nested.nested.nested.nested.nested.nested.nested.superNested);
    }
}


const deepCopy = (oldObj) => {

    // Base case, we get a root value
    if(typeof oldObj !== 'object') {
        return oldObj;
    };

    // Recursive case, make a new object that's a deep
    // copy of the nested object
    let newObject = {};

    for(let item in oldObj) {
        newObject[item] = deepCopy(oldObj[item]);
    }

    // Always return a deep copy object nesting
    return newObject;

}

let clonedObj1 = deepCopy(obj1);

clonedObj1.aFunction2(); // I am super nested

obj1.nested.nested.nested.nested.nested.nested.nested.superNested = 'I am the changed super nested';

obj1.aFunction2(); // I am the changed super nested

clonedObj1.aFunction2(); // I am super nested

