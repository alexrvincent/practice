/* Say we have


const obj = {
    a: {
        b: {
            c: 1
        }
    }
}

and we wish to clone it into and object 'clone';

We can do so using 'Object.assign({}, obj)' or {...obj}. But this only does a shallow copy? How can we
do a deep copy?

*/

const deepCopy = (obj) => {
    return JSON.parse(JSON.stringify(obj));
}

const obj = {
    a: {
        b: {
            c: 1
        }
    }
}

const clone = deepCopy(obj);

console.log(obj.a.b.c); // should print 1
console.log(clone.a.b.c); // should print 1

clone.a.b.c = 100;

console.log(obj.a.b.c); // should print 1
console.log(clone.a.b.c); // should print 100


