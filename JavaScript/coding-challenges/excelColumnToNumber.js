/*
A = 1
B = 2
...
Z = 26
AA = 27
AB = 28
..
AZ = 52
*/

var titleToNumber = function(s) {

    // Keep track of the 
    let sum = 0;
    
    // Iterate through the string almost as a base 26 number
    for (let i = 0; i < s.length; ++i) {

        // Formula for an individual character in this kind of string is
        // ( ALPHA_LETTER_VAL * (26 ^ LETTERS_TO_LEFT_OF_THIS) ) 
        // Add up all the individual characters of this string
        sum = sum + (getNumericValue(s[i]) * Math.pow(26, (s.length-1)-i));
    }

    // Return its result
    return sum;
};

// Returns the alphanumeric letter value via checking the difference from a known value
const getNumericValue = (char) => {
	return Math.abs('A'.charCodeAt(0) - char.charCodeAt(0)) + 1;
}