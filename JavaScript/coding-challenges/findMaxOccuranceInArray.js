// var majorityElement = function(nums) {

//     nums.sort((a,b) => a-b);
    
//     let currentMajority = nums[0];
//     let count = 1;
    
//     if(nums.length === 1) return currentMajority;
        
//     for (let i = 1; i < nums.length; ++i) {
   
//         if(nums[i] === currentMajority){
//             ++count;
//             if(count > (nums.length/2)) return currentMajority;
//         }
//         else {
//             currentMajority = nums[i];
//             count = 1;
//         }
//     }
// };

var majorityElement = function(nums) {

    nums.sort((a,b) => a-b);
    return nums[Math.floor(nums.length/2)]
};