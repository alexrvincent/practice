// Run using 'node ./fizzbuzz.js [your-amount-here]'

const fizzbuzz = (amount) => {
    for (var i = 1; i <= amount; ++i) {
        var output = '';
        if ( i % 3 === 0 ) output += 'fizz';
        if ( i % 5 === 0 ) output += 'buzz';
        console.log(`${i} - ${output}`);
    }
}

fizzbuzz(process.argv[2] || 100);