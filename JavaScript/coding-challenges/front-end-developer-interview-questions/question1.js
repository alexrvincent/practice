/**
 * Created by Alex on 4/2/2018.
 */
// Run using 'node ./question1.js'
// Question: What will be the output of the code below

var foo = 10 + '20';
console.log(foo);
console.log("Foo is '1020' as a string because + is a concatenation operator when a string is involved.");

console.log(0.1 + 0.2 == 0.3);
console.log("False because of JavaScript's lack of precision. To fix this, convert the numbers to integers then divide. Or to a .toFixed");

console.log(+(0.1 + 0.2).toFixed(2) == 0.3);
console.log("True because the value is truncated.");

