/**
 * Created by Alex on 4/2/2018.
 */
// Run using 'node ./question2.js'

// How would you make this work?

// add(2, 5);  returns 7
// add(2)(5); returns 7

const add = (a, b) => {
    // If there exists two parameters, return their sum
    if (a && b) { return a + b; }

    // Otherwise return a function that will process the second parameter with the first through closure context
    else { return (b) => { return a + b; } }
};

console.log(add(2, 5));
console.log(add(2)(5));




