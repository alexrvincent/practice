/**
 * Created by Alex on 4/2/2018.
 */
// Run using 'node ./question3.js'

// Question: What value is returned from the following statement?

// "i'm a lasagna hog".split("").reverse().join("");

// 1) .split("")
// This will turn "i'm a lasagna hog" into an array of characters split at "" (which is nothing).

// 2) .reverse()
// This should reverse the array in place

// 3) .join("")
// This will join the array items into a string with no space between the characters

// The value should be 'goh angasal a m'i'

console.log("i'm a lasagna hog".split("").reverse().join(""));







