/**
 * Created by Alex on 4/2/2018.
 */
// Run using 'node ./question3.js'

// Question: What is the value of window.foo?

// ( window.foo || ( window.foo = "bar" ) );

// The value of window.foo should be 'bar'. The expression above evaluates whether window.foo or ( window.foo = "bar")
// are true. window.foo evaluates to false (as it's null), and window.foo = "bar" values to true because the assignment operator here
// returns "bar" which is a truthy value. While the entire expression returns true, the assignment still took place. Since window is a global
// object that be assigned any value, it retains the bar from the expression.

// To demonstrate this process in node.js, I've replaced window with 'this' - both refer to the 'global' instance variable.

( this.foo || ( this.foo = "bar" ) );
console.log(this.foo);





