/**
 * Created by Alex on 4/2/2018.
 */
// Run using 'node ./question3.js'

// Question: What is the outcome of the two alerts below?

// var foo = "Hello";
// (function() {
//   var bar = " World";
//   alert(foo + bar);
// })();
//
// alert(foo + bar);

// The result of the first alert should be "Hello World". The global context holds the value "Hello" which is accessible
// to the local context of the inline function expression.

// The second alert will execute because the context of bar is undefined at that level. This will instead throw an error.
// If it doesn't throw and error, it will at least alert "Hello undefined".

var foo = "Hello";
(function() {
  var bar = " World";
  alert(foo + bar);
})();

alert(foo + bar);






