/**
 * Created by Alex on 4/2/2018.
 */
// Run using 'node ./question3.js'

// Question: What is the value of foo.length?
//
//     var foo = [];
//     foo.push(1);
//     foo.push(2);

// The value of foo.length is 2.

var foo = [];
foo.push(1);
foo.push(2);

console.log(foo.length);




