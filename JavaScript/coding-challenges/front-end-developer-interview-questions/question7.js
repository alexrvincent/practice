/**
 * Created by Alex on 4/2/2018.
 */
// Run using 'node ./question3.js'

// 7) What is the value of foo.x?
//
//     var foo = {n: 1};
//     var bar = foo;
//     foo.x = foo = {n: 2};

// Line 1: Create an object (A) and assign a reference to it. Name that reference 'foo'.
// Line 2: Copy the reference to A (foo) into a new variable called 'bar';
// Line 3: Create a new object (B) and assign 'foo' to reference it. Give the object A property 'x' and place a reference to B in that value.

// This is what the memory should look like

// foo: '0x1234' (Memory address referencing object)
// bar: '0x4321' (Memory address referencing object)
// 0x1234: { n: 2 }
// 0x4321: { n: 1, x: '0x1234' }

// The value of foo.x at the end of this should be undefined. This is because the object to which foo now references (B)
// no longer has an attribute x. When foo.x was assigned a value, the foo in question referenced object A. Foo now references
// object B and therefore has an undefined 'x'. However, bar.x should print { n: 2 } because it references the original object A
// that was assigned an x value

// This goes to show that inline assignments don't chain or depend on one another! They're entirely separate!

var foo = {n: 1};
var bar = foo;
foo.x = foo = {n: 2};

console.log(foo); // { n : 2 }
console.log(foo.x); // undefined
console.log(bar); // { n: 1, x : { n : 2 } }





