/**
 * Created by Alex on 4/2/2018.
 */
// Run using 'node ./question3.js'

// 8) What does the following code print?
//
//     console.log('one');
//     setTimeout(function() {
//       console.log('two');
//     }, 0);
//     console.log('three');


// Line 1: Print the string 'one'
// Line 2: Call the global function setTimeout, which prints "two" after zero milliseconds.
// Line 3: Print the string 'three'

// The following code will print:
// one
// three
// two

// This is because setTimeout, and any other async function always executes after the main code run though in the next event loop.
// Even though its timeout is set to 0, that function and any other like it get reserved for after the run through in the next event loop.

console.log('one');
setTimeout(function() {
  console.log('two');
}, 0);
console.log('three');







