/**
 * Created by Alex on 4/2/2018.
 */
// 9) What is the difference between these four promises?
//
//     doSomething().then(function () {
//       return doSomethingElse();
//     });
//
//     doSomething().then(function () {
//       doSomethingElse();
//     });
//
//     doSomething().then(doSomethingElse());
//
//     doSomething().then(doSomethingElse);

// It's implied that doSomething() is a function that returns a promise. the .then() is expected to intercept
// the value of this and execute a callback with that value.

// Here's the main difference between them:
// Promise 1: Executes an anonymous function that returns the value of the function doSomethingElse() after doSomething() resolves.
// Promise 2: Executes an anonymous function which calls doSomethingElse() after doSomething() resolves.
// Promise 3: Executes the function doSomethingElse() not as a callback but inline after doSomething() resolves
// Promise 4: Executes the function doSomethingElse() as a callback after doSomething() resolves.



let doSomething = () => {
    return new Promise((resolve, reject) => {
        resolve('I resolved!');
    });
};

let doSomethingElse = () => {
    console.log("I did something else!");
    return "Returned value";
};

doSomething().then(function () {
  return doSomethingElse();
});

doSomething().then(function () {
  doSomethingElse();
});

doSomething().then(doSomethingElse());

doSomething().then(doSomethingElse);