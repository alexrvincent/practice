const generateFibonacci = (n=1) => {

    let results = [0,1];
  
    for(let i = 0; i <= n-2; ++i) {
    results.push(results[i] + results[i+1]);
    }

    return results;
  
}

const generateFibonacciRecursively = (n=0) => {
    
    if (n === 0) return [0];
    if (n === 1) return [0, 1];

    let oneBelow = generateFibonacciRecursively(n-1);

    return [...oneBelow,  oneBelow[n-1] + oneBelow[n-2]];
  
}

console.log(generateFibonacci(10));

console.log(generateFibonacciRecursively(10));