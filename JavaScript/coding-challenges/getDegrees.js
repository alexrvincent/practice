// ex "8:45"
const getDegrees = (time) => {

	let hour = (time.split(":")[0] != 12) ? (time.split(":")[0] * 5) : 0;
  let minutes = time.split(":")[1];
     
  return (Math.max(hour, minutes) - Math.min(hour, minutes)) * 6;
  
}

console.log(getDegrees("9:50"))