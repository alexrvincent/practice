function getFileExtension(str){
    let extension = str.split(".");

    if(extension.length === 1) return false;
    else return extension[1];
}

console.log(getFileExtension("file.txt"));
console.log(getFileExtension("nofileextension"));
console.log(getFileExtension(".hi"));