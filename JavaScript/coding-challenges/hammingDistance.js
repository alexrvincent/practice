var hammingDistance = function(x, y) {

    // Convert the integers to a nice array of bits form.
	let bitX = convertToBitArray(x);
    let bitY = convertToBitArray(y);
        
    let distance = 0;
        
    // Compare each index; a difference means increase the sum.
    for(let i = 0; i <= 32; ++i) {
        if( bitX[i] !== bitY[i] ) ++distance;
    }

    return distance;    
};

// Converts an integer into an array of it its 32-bit int form
// Bits in the array are represented as strings
var convertToBitArray = function (int) {

    // Convert the int into a string of its bits
    let bitInt = int.toString(2)
    
    // Initialize a new array of length 32 - bitInt.length, filling the empty spots with zeros
    let arr = new Array(32 - bitInt.length).fill("0");
  
    // Push on the remaining bits from the string onto the array to make a full 32-bit array
    for(let i = 0; i < bitInt.length; ++i) {
        arr.push(bitInt[i]);
    }
  
    return arr;
  
};