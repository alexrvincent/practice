const howManyInstancesOf = (character, value) => {
    let instances = 0;
    for (let selectedChar of value) if (selectedChar === character) ++instances
    return instances;
}

console.log(howManyInstancesOf('c', 'character'));