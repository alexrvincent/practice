/* 

Make the following work via inheritance:

const a = function (x) {
    this.x = x;
}

const b = function(x, y) {
    this.y = y;
}

const newB = new b('One', 'Two');

newB.getX(); // 'One'
newB.getY(); // 'Two';

*/

const a = function (x) {
    this.x = x;
    
    this.getX = function() {
        return this.x;
    }
}

const b = function(x, y) {

    a.call(this, x); // inherit from a by redirecting any references to unknown
                     // fields / methods to an instance of a with x.

    this.y = y;

    this.getY = function() {
        return this.y;
    };
}

const newB = new b('One', 'Two');

console.log(newB.getX()); // 'One'
console.log(newB.getY()); // 'Two';