function isNumberEven(i) {
    return i % 2 === 0;
}

console.log(isNumberEven(1));
console.log(isNumberEven(2));
console.log(isNumberEven(3));