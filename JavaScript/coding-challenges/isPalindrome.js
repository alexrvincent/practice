const isPalindrome = (value) => {
    if(typeof(value) !== 'string') return false;

    let start = 0;
    let end = value.length-1;

    while(start < end) {
        if(value[start] !== value[end]) return false;
        ++start;
        --end;
    }

    return true;
}

console.log(isPalindrome('wow')); // true
console.log(isPalindrome('440044')); // true
console.log(isPalindrome("hello")); // false
console.log(isPalindrome(5)); // false
console.log(isPalindrome("i")); // true
console.log(isPalindrome("level")); // true


const isPalindrome2 = (value) => {
    if(typeof(value) !== 'string') return false;

    for(let start = 0, end = value.length -1 ; start < end; ++start, --end) {
        if(value[start] !== value[end]) return false ;
    }

    return true;
}

const isPalindrome3 = (value) => {
    return value === value.split('').reverse().join('');
}