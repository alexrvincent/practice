/* 
    1
   / \
  2   4
    /
   3

Pre Order: [1, 2, 4, 3] 
*/

function TreeNode(val) {
     this.val = val;
     this.left = this.right = null;
}

var root = new TreeNode(1);
root.left = new TreeNode(2);
root.right = new TreeNode(4);
root.right.left = new TreeNode(3);

var preorderTraversalRecursive = function(root) {

    let preOrder = [];
    
    // Return an empty array when we hit a null node
    if(root === null) return preOrder;
    
    else {
        
        // Visit the node the pushing it onto an array
        preOrder.push(root.val);
        
        // Push the entire left tree onto the array
        if(preorderTraversalRecursive(root.left).length) { 
            preOrder.push(...preorderTraversalRecursive(root.left)) 
        };
        
        // Push the entire right tree onto the array
        if(preorderTraversalRecursive(root.right).length) { 
            preOrder.push(...preorderTraversalRecursive(root.right)) 
        };
    }
    
    // Return the entire tree
    return preOrder;
    
};

var preorderTraversalIterative = function(root) {

    // Our return values
    let preOrder = [];

    // Handle null roots
    if (root === null) return preOrder;

    else {

        // Otherwise use a stack
        let stack = [];
        
        // Start by pushing the non-null root to the stack
        stack.push(root);

        // While the stack contains items
        while (stack.length) {

            // Pop the item
            let topItem = stack.pop();

            if (topItem === null) continue;

            preOrder.push(topItem.val);
            stack.push(topItem.right);
            stack.push(topItem.left);
            
        }

    }

    return preOrder;
}

console.log(preorderTraversalRecursive(root));
console.log(preorderTraversalIterative(root));