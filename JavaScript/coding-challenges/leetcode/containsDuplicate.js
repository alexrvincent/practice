// My first attempt. It's realllly bad because we copy the array :(
var containsDuplicate = function(nums) {
    
    // For every item in the array
    for(let i = 0; i < nums.length; ++i) {
        
        // If the item is in the subset of the array (a.k.a, array - the first element) return true
        if(nums.slice(i+1).indexOf(nums[i]) !== -1) return true;

        console.log(nums.slice(i+1));
    }
    
    // Return false by default
    return false;
};

// Second attempt. It's better but still really slow.
var containsDuplicate2 = function(nums) {
    
    // For every item in the array
    for(let i = 0; i < nums.length; ++i) {

        for(let j = 0; j < i; ++j)
    
        if(nums[i] === nums[j]) return true;
    }
    
    // Return false by default
    return false;
};

// My third attempt which was muuuch faster. Remember sets are guaranteed to not have duplicates!
var containsDuplicate3 = function(nums) {
    return (new Set(nums).size) !== nums.length;
};

// If you're reading this in the future. Know that Leetcode recommends using 
// 1) A sorting algorithm first then a loop through they array
// 2) Using a data structure like a set or hashset which guarantees non duplicates.


console.log(containsDuplicate3([1,2,3]))