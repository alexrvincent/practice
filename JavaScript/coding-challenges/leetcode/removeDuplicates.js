// My first answer
var removeDuplicates = function(nums) {
    
    // Return 0 for empty arrays
    if (!nums.length) return 0;
        
    // Otherwise for every element in the array ...
    for(let i = 0; i < nums.length; ++i) {

        // Keep track of how many duplicates we need to delete
        let deleteItems = 0; 
        
        // Traverse the remaining items in the list
        for(let j = i + 1; j < nums.length; ++j) {

            // As long as we find duplicates, increment the delete counter otherwise break
            if(nums[i] !== nums[j]) break;
            else ++deleteItems  
                
        }
        
        // Delete the number of duplicates we found
        nums.splice(i, deleteItems)
    }
    
    return nums.length;
};

// My answer but better from someone
var removeDuplicates2 = function(nums) {
    for (let i = 0; i < nums.length; ++i) {
      while(nums[i] === nums[i + 1]) {
        nums.splice(i + 1, 1);
      }
    }
  
    return nums.length;
};

// Leetcode's solution but in JS - its dumb because it doesn't mutate the object just how
// long the array should be.
var removeDuplicates3 = function (nums) {
    if (nums.length == 0) return 0;
    let i = 0;
    for (let j = 1; j < nums.length; j++) {
        if (nums[j] != nums[i]) {
            i++;
            nums[i] = nums[j];
        }
    }
    return i + 1;


}

console.log(removeDuplicates([1,1,2,2,3,3]));

