// My original solution
var rotate = function(nums, k) {
    for(let i = nums.length - k; i < nums.length; ++i) {
        nums.unshift(nums.pop())
    }
};

let nums = [1,2,3,4,5,6];
rotate(nums, 3);
console.log(nums);