
// First answer! I sort the array because I know duplicates will line up.
// Then its just a single pass through to make sure the pairs are together
var singleNumber = function(nums) {
    
    // Sort the array
    nums.sort((a,b) => a-b);

    for (let i = 0; i < nums.length; i+=2) {
        if(nums[i] !== nums[i+1]) return nums[i];
    }
};

// I don't like this, but it's the best solution I've seen supplied by Leetcode. Basically
// it does a bitwise XOR. still need to figure out why.
var singleNumber = function(nums) {
    let res = nums[0], len = nums.length, i = 1
    for(i; i < len; i++) {
        res ^= nums[i]
    }
    return res
};

console.log(singleNumber([2,1,4,6,8,9,3,6,9,4,8,1,2]))