// Given
function TreeNode(val) {
    this.val = val;
    this.left = this.right = null;
}

// Make this tree
//     3
//    / \
//   9  20
//     /  \
//    15   7

let root = new TreeNode(3);
root.left = new TreeNode(9);
root.right = new TreeNode(20);
root.right.left = new TreeNode(15);
root.right.right = new TreeNode(7);

const maxDepth = (node) => {

    // Base case: We reach a leaf node
    if (node === null) return 0;

    // Get the depth of the left tree
    let leftDepth = maxDepth(node.left);

    // Get the depth of the right tree
    let rightDepth = maxDepth(node.right);

    // Return whichever subtree was larger, plus the root to make
    // this tree (or subtrees) depth
    return Math.max(leftDepth, rightDepth) + 1;
}

console.log(maxDepth(root));