const a = [1,3,5,7];
const b = [2,4,6,8];

const mergeTwoSortedArrays = (arr1, arr2) => {
    return [...arr1, ...arr2].sort()
}

console.log(mergeTwoSortedArrays(a,b));