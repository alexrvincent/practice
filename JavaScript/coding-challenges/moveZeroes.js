// Input: [0,1,0,3,12]
// Output: [1,3,12,0,0]

// Do it in place

const moveZeroes = (nums) => {

    for(let i = nums.length - 1; i >= 0; --i) {
        if(nums[i] === 0) {
            nums.push(nums.splice(i, 1));
        }
    }
    
}
