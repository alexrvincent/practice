/* Create a function pipe that performs left-to-right function composition by returning a function that accepts one argument. 

const square = v => v * v
const double = v => v * 2
const addOne = v => v + 1
const res = pipe(square, double, addOne)
res(3) // 19; addOne(double(square(3)))

*/

// Pipe accepts a list of functions which we gather using the rest operator
const pipe = (...funcs) => {

    // We need to return a function that, when executed with an initial value,
    // will return the executed sequence from that start value
    return (initValue) => {

        // We define a reducer function which takes an accumulator (think of it like a
        // local incremented variable), and the current value at the index of our initial
        // array (in this case a function)
        const reducer = (accumulator, currentFunc) => {

            // All this reducer does is call the current function in the array
            // with the current accumulator value and return it to the next iteration
            return currentFunc(accumulator);
        }

        // Finally reduce the array of functions by passing the function to execute 
        // on each iteration and an initial value to start the accumulator with. Return
        // the actual value
        return funcs.reduce(reducer, initValue);
    }
    
}

// ES6 shorthand version
const pipe2 = (...fns) => x => fns.reduce((v, fn) => fn(v), x)

const square = v => v * v;
const double = v => v * 2;
const addOne = v => v + 1;
const res = pipe(square, double, addOne);
console.log(res(3));// 19; addOne(double(square(3)))