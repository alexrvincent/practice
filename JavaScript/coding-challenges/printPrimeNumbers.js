function printPrimeNumbers(amount){
    let count = amount;
    let numbers = [];
    for (let i = 2; (count > 0); ++i){
        if(isPrime(i)) {
            numbers.push(i);
            --count;
        }
    }
    console.log(numbers);
}

function isPrime(number) {
    for (let i = 2; i < number; ++i) {
        if (number % i === 0) return false;
    }
    return true;
}