/* Print numbers 1 - n without using a loop */

const printWithoutLoop = (n) => {
    let arr = new Array(n);
    arr.fill(1);

    console.log(...arr);

}

printWithoutLoop(10);