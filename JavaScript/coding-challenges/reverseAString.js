const reverseAString = (str) => {
    return str.split("").reverse().join("");
}

const reverseAString2 = (str) => {
    let newStr = '';
    for (let i = str.length - 1; i >= 0; --i ) newStr += str[i];
    return newStr;
}

const reverseAString3 = (str) => {
    if (str === "") {
        return "";
    }
    else {
        return reverseAString3(str.substr(1)) + str.charAt(0);
    }
}

console.log(reverseAString3("Hello!"));



