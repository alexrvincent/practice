// Given nums = [2, 7, 11, 15], target = 9,

// Because nums[0] + nums[1] = 2 + 7 = 9,
// return [0, 1].

var twoSum = function(nums, target) {
        
    let hashMap = new Map(nums.map((item, index) => [item, index]));
    
    for (let i = 0; i < nums.length; ++i) {
    
    		let foundVal = hashMap.get(target - nums[i]);
        
    		if(foundVal) {
        		return [i, foundVal]
        }
    }
    
};


var twoSum = function(nums, target) {
        
    // Outer loop keeps track of the 
    for(let i = 0; i < nums.length; ++i) {
        
        for (let j = i + 1; j < nums.length; ++j) {
            
            if (nums[j] === (target - nums[i]) ) {
                return [i, j]    
            }
        }
        
    }
    
};

console.log(twoSum([3, 2, 4], 6));