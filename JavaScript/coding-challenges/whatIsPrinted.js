/* Problem 1 */

(function() {
    var a = b = 5;
 })();
 console.log(b);

 // Answer: 
 // 5

 /* Problem 2 */

 function test() {
     console.log(a); 
     console.log(foo());
     console.log(bar);
     //console.log(baz);

     var a = 1;
     function foo() {
         return 2;
     }

     var bar = function baz() {
         return 10;
     }
 }

 test();

 // Answer: 
 // undefined because only variable declaration gets hoisted
 // 2 because function declaration and body gets hoisted
 // undefined because variable gets hoised but not its assigned value
 // ReferenceError because function named expressions don't hoist their value at all.

 /* Problem 3 */

const arr = [1, 2, 3, 4, 5]
for (var i = 0; i < arr.length; ++i) {
  setTimeout(() => {
    console.log("arr[" + i + "]: " + arr[i])
  }, 0)
}

// Answer: 
// It prints a list of arr[5]: undefined
// 1) Because of the use of var here, the variable i is declared in the global scope
// (or in this case the scope the for loop is defined in a.k.a the global scope)
// 2) When the setTimeouts attempt to evaluate i, the closure
// of the global function scope gives them definition of i and its last value (5)
// not its scoped value at the instance it was meant to be called during the for loop iteration.
//  This is because 'var' doesn't have block level scope.
// 3) Use let to fix this because it retains its block level scope value.

/* Problem 4 */

const obj = {
    x: 1,
    getX() {
        const inner = function() {
            console.log(this.x);
        }
        inner();
    }
};

obj.getX();

// Answer:
// Prints undefined because 'this' in inner function does not reference object instance.
// There's a few ways to fix this
// 1) Define inner with an arrow function. This will be implicitly bound to its context;
// 2) Define inner with let and do inner = inner.bind(this) after the definition. 
// 3) Make a variable called 'that' and assign it 'this'