// Example 2:
// This will print undefined also, as only the variable declaration is hoisted to the top
console.log(myHoistedVariable);
var myHoistedVariable = "I don't get hoisted because only the declaration does :(";