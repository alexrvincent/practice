// Example 3:
// This will throw an error because let nor const variables are hoisted
console.log(myLetVariable);
console.log(myConstVariable)
let myLetVariable = "I am let!";
const myConstVariable = "I am const!"