
// Example 4
// Prints 'function declaration ran' because function declarations are hoisted
myFunctionDeclaration()

function myFunctionDeclaration() {
    console.log("Function declaration ran");
}


// Errors out because function expressions are not hoisted
myFunctionExpression();

var myFunctionExpression = function() {
    console.log("Function expression ran");
}