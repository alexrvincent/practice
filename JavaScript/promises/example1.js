
var myCallback = function() {
    console.log("Hello World!");
}

// Async with callback
var myAsyncCallbackFunction = function(callback) {
    setTimeout(callback, 100);
}
myAsyncCallbackFunction(myCallback);

// Just a promise
var myAsyncPromiseFunction = new Promise(function(resolve, reject){
    setTimeout(resolve, 2000);
});

myAsyncPromiseFunction.then(myCallback);
