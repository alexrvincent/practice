class ClassAnimal {
    eat() {
        console.log('Yum!');
    }
}

console.log(new ClassAnimal())
// ------------------------------------------

function ConstructorAnimal() {

}

ConstructorAnimal.prototype.eat = function () {
    console.log('Yum!');
}

console.log(new ConstructorAnimal())

// ------------------------------------------

let AnimalPrototype = {
    eat: function() {
        console.log('Yum!')
    }
}

function FactoryAnimal() {
    return Object.create(AnimalPrototype);
}
