var Person = function(name) {
    this.name = name;
    this.canTalk = true;
}

Person.prototype.greet = function() {
    if(this.canTalk) {
        console.log("Hi, I'm " + this.name);
    }
}

var bob = new Person("bob");
bob.greet();

