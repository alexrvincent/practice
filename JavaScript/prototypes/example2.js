let Person = function (name, age) {
    this.name = name;
    this.age = age;
    this.speak = function() {
        if (this.name === undefined || this.age === undefined) {
            console.log("I don't know who I am yet!")
        }
        else {
            console.log(`Hello! I'm ${this.name} and I'm ${this.age} years old.`)
        }
    }
}


/* There are lots of ways to make inheritance work */

// 1. Create a base object and have another object's proto point to it.

let PersonBase = new Person();
let Bob = {};

Object.setPrototypeOf(Bob, PersonBase); // Both of these do the same thing!
Bob.__proto__ = PersonBase;             // Both of these do the same thing!

Bob.speak(); // bob inherits the speak function but because he wasn't initialized with
             // any fields, he doesn't know who he is!

// 2. Create a new object using a base object as a template / fallback

let Jane = Object.create(PersonBase); // Basically a short
Jane.speak();

// 3. Set the prototype field on a function constructor and instantiate with new
function Man() {
    this.hairy = true
}

Man.prototype = new Person();

let Sam = new Man();

